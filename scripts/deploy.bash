#!/bin/bash

while [[ $# > 1 ]]
do
key="$1"

case $key in
    -s|--server)
    SERVER="$2"
    shift
    ;;
    -u|--user)
    SERVERUSER="$2"
    shift
    ;;
    -b|--branch)
    BRANCH="$2"
    shift
    ;;
    -h|--help)
    SHOWHELP="$2"
    shift
    ;;
    *)
    ;;
esac
shift
done

DEFAULT_SERVER="packages.getrefm.com"
DEFAULT_SERVERUSER="getrefm"
DEFAULT_BRANCH="master"

if [ -z ${SERVER} ]; then
  SERVER=${DEFAULT_SERVER}
fi

if [ -z ${SERVERUSER} ]; then
  SERVERUSER=${DEFAULT_SERVERUSER}
fi

if [ -z ${BRANCH} ]; then
  BRANCH=${DEFAULT_BRANCH}
fi

if [ -z ${SERVER} ] || [ -z ${SERVERUSER} ] || [ -z ${BRANCH} ] || [ ${SHOWHELP} ]; then 
  echo "Usage: ./deploy.sh      -s <serverip> -u <serveruser> -b <branch> -h"; 
  echo "-s  or --server         ip address of the server, optional, default value is ${DEFAULT_SERVER}";
  echo "-u  or --user           server user name, optional, default value is ${DEFAULT_SERVERUSER}";
  echo "-b  or --branch         branch to be updated and executed, optional, default value is ${DEFAULT_BRANCH}";
  echo "-h  or --help           show this help";
  exit
fi

echo "SERVER: ${SERVER}"
echo "USER: ${SERVERUSER}"
echo "BRANCH: ${BRANCH}"

ssh -o 'StrictHostKeyChecking no' -t -t -A ${SERVERUSER}@${SERVER} BRANCH="${BRANCH}" "bash -s" << "ENDSSH"
if [ -z "$SSH_AUTH_SOCK" ] ; then
    eval `ssh-agent -s`
fi
ssh-add ~/.ssh/id_rsa
source ~/.profile
cd ~/packages
git reset --hard HEAD
git fetch --all
git checkout ${BRANCH}
git pull origin ${BRANCH}
composer install -n --prefer-dist
exit
ENDSSH
